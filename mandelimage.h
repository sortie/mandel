#ifndef MANDELIMAGE_H
#define MANGELIMAGE_H

class MandelImage
{
public:
	MandelImage(size_t stride, unsigned width, unsigned height);
	MandelImage(MandelImage* inner);
	~MandelImage();

public:
	uint8_t* GetData() const { return data; }
	size_t GetStride() const { return stride; }
	unsigned GetWidth() const { return width; }
	unsigned GetHeight() const { return height; }
	MandelImage* GetInner() { return inner; }

public:
	bool Matches(size_t stride, unsigned width, unsigned height) const 
	{
		return this->stride == stride &&
		       this->width == width &&
		       this->height == height;
	}

public:
	MandelImage* next;
	float xoffset;
	float yoffset;
	float xlength;
	float ylength;
	unsigned maxiters;
	unsigned lineoffset;
	unsigned lineskip;
	size_t chunksleft;

private:
	uint8_t* data;
	size_t stride;
	unsigned width;
	unsigned height;
	MandelImage* inner;

};

#endif
