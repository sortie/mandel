#ifndef MANDELCONTROLLER_H
#define MANDELCONTROLLER_H

#include "mandelprovider.h"

class MandelController : public MandelProviderListener
{
public:
	virtual ~MandelController() { }
	virtual void OnResize(unsigned width, unsigned height) = 0;
	virtual void Schedule() = 0;


};

#endif
