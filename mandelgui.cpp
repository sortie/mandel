#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <string.h>
#include <cairo.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "mandelimage.h"
#include "mandelcolorer.h"
#include "mandelprovider.h"
#include "mandelgui.h"

gboolean MandelGUI__OnButtonPress(GtkWidget* widget, GdkEventButton* event, gpointer user)
{
	return ((MandelGUI*) user)->OnButtonPress(widget, event);
}

gboolean MandelGUI__OnDraw(GtkWidget* widget, cairo_t* cr, gpointer user)
{
	return ((MandelGUI*) user)->OnDraw(widget, cr);
}

#if GTK_MAJOR_VERSION <= 2
gboolean MandelGUI__OnExpose(GtkWidget* widget, GdkEventExpose* event, gpointer user)
{
	cairo_t* cr = gdk_cairo_create(widget->window);
	cairo_rectangle(cr, event->area.x, event->area.y, event->area.width, event->area.height);
	cairo_clip(cr);
	bool result = ((MandelGUI*) user)->OnDraw(widget, cr);
	cairo_destroy(cr);
	return result;
}
#endif

gboolean MandelGUI__OnScroll(GtkWidget* widget, GdkEventScroll* event, gpointer user)
{
	return ((MandelGUI*) user)->OnScroll(widget, event);
}

void* RenderThread(void* user)
{
	MandelProvider* provider = (MandelProvider*) user;
	provider->MainLoop();
	delete provider;
	return NULL;
}

MandelGUI::MandelGUI(MandelColorer* colorer, MandelProvider* provider,
                     unsigned windowwidth, unsigned windowheight)
{
	this->windowwidth = windowwidth;
	this->windowheight = windowheight;
	this->colorer = colorer;
	this->provider = provider;
	this->queuefront = NULL;
	this->queuelast = NULL;
	this->numscheduled = 0;
	this->xcenter = -0.5f;
	this->ycenter = 0.0f;
	this->xwidth = 4.0;
	this->areawidth = 100;
	this->areaheight = 100;
	this->currentframe = NULL;
	this->quality = 25U;
	pthread_mutex_init(&queuelock, NULL);

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	g_signal_connect_swapped(G_OBJECT(window), "destroy",
	                         G_CALLBACK(gtk_main_quit), NULL);

	gtk_window_set_title(GTK_WINDOW(window), "mandel");
	gtk_window_set_default_size(GTK_WINDOW(window), windowwidth, windowheight);
	gtk_widget_add_events(GTK_WIDGET(window), GDK_CONFIGURE);

	screen = gtk_drawing_area_new();

	gtk_container_add(GTK_CONTAINER(window), screen);

	gtk_widget_add_events(screen, GDK_SCROLL_MASK);
	gtk_widget_add_events(screen, GDK_BUTTON_PRESS_MASK);

	g_signal_connect(G_OBJECT(screen), "button_press_event",
	                 G_CALLBACK(MandelGUI__OnButtonPress), this);
#if 3 <= GTK_MAJOR_VERSION
	g_signal_connect(G_OBJECT(screen), "draw",
	                 G_CALLBACK(MandelGUI__OnDraw), this);
#else
	g_signal_connect(G_OBJECT(screen), "expose_event",
	                 G_CALLBACK(MandelGUI__OnExpose), this);
#endif
	g_signal_connect(G_OBJECT(screen), "scroll_event",
	                 G_CALLBACK(MandelGUI__OnScroll), this);

	gtk_widget_show_all(window);

	provider->SetListener(this);
	pthread_t renderthread;
	pthread_create(&renderthread, NULL, RenderThread, provider);
}

MandelGUI::~MandelGUI()
{
	gtk_widget_destroy(window);
	provider->Halt();
	// TODO: Actually wait for the provider to shut down here.
	pthread_mutex_destroy(&queuelock);
	while ( queuefront )
	{
		MandelImage* todelete = queuefront;
		queuefront = queuefront->next;
		delete todelete;
	}
	delete currentframe;
}

bool MandelGUI::OnButtonPress(GtkWidget* widget, GdkEventButton* event)
{
	if ( event->type != GDK_BUTTON_PRESS ) { return true; }

	float factor = 0.75f;
	float inverse = 1.0f / factor;

	float newcenterx = TranslateX(event->x);
	float newcentery = TranslateY(event->y);

	switch ( event->button )
	{
	case 1: xwidth *= factor; break;
	case 3: xwidth *= inverse; break;
	}

	xcenter = newcenterx;
	ycenter = newcentery;
	
	return true;
}

void MandelGUI::QueueRedraw()
{
	gtk_widget_queue_draw(GTK_WIDGET(window));
}

float MandelGUI::LengthX() const
{
	return xwidth;
}

float MandelGUI::LengthY() const
{
	float ratio = (float)areaheight / (float)areawidth;
	return LengthX() * ratio;
}

float MandelGUI::OffsetX() const
{
	return -LengthX()/2.0f + xcenter;
}

float MandelGUI::OffsetY() const
{
	return -LengthY()/2.0f + ycenter;
}

float MandelGUI::TranslateX(unsigned x) const
{
	return OffsetX() + (float) x * LengthX() / (float) areawidth;
}

float MandelGUI::TranslateY(unsigned y) const
{
	float corner = OffsetY() + LengthY();
	return corner - (float) y * LengthY() / (float) areaheight;
}

bool MandelGUI::OnDraw(GtkWidget* widget, cairo_t* cr)
{
	cairo_format_t format = CAIRO_FORMAT_RGB24;

	// TODO: May need a lock here.
	if ( numscheduled < 2 )
	{
#if 3 <= GTK_MAJOR_VERSION
		int width = gtk_widget_get_allocated_width(widget);
		int height = gtk_widget_get_allocated_height(widget);
#else
		GtkAllocation widgetalloc;
		gtk_widget_get_allocation(widget, &widgetalloc);
		int width = widgetalloc.width;
		int height = widgetalloc.height;
#endif
		size_t stride = cairo_format_stride_for_width(format, width);

		areawidth = width;
		areaheight = height;

		MandelImage* image = new MandelImage(stride, width, height);
		size_t wstride = image->GetStride();
		unsigned wheight = image->GetHeight();
		uint8_t* wdata = image->GetData();
		image->xlength = LengthX();
		image->ylength = LengthY();
		image->xoffset = OffsetX();
		image->yoffset = OffsetY();
		image->maxiters = quality;
		image->lineoffset = 0;
		image->lineskip = 1;

		pthread_mutex_lock(&queuelock);
		numscheduled++;
		pthread_mutex_unlock(&queuelock);

		provider->Schedule(image);
	}

	delete currentframe; currentframe = NULL;
	pthread_mutex_lock(&queuelock);
	currentframe = queuefront;
	if ( queuefront ) { queuefront = queuefront->next; }
	if ( !queuefront ) { queuelast = NULL; }
	pthread_mutex_unlock(&queuelock);

	if ( currentframe )
	{
		colorer->Color(currentframe);
		size_t stride = currentframe->GetStride();
		unsigned width = currentframe->GetWidth();
		unsigned height = currentframe->GetHeight();
		uint8_t* data = currentframe->GetData();
		cairo_surface_t* surface;
		surface = cairo_image_surface_create_for_data(data, format, width,
		                                              height, stride);
		cairo_set_source_surface(cr, surface, 0.0, 0.0);
		cairo_paint(cr);
		cairo_surface_destroy(surface);
		return true;
	}

	cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
	cairo_set_font_size(cr, 30.0);
	cairo_move_to(cr, 20.0, 40.0);
	cairo_show_text(cr, "Rendering...");
	return true;	
}

bool MandelGUI::OnScroll(GtkWidget* widget, GdkEventScroll* event)
{
	unsigned change = (quality < 25) ? 1 : 5;
	if ( event->direction == GDK_SCROLL_UP && quality < 800 ) { quality += change; }
	if ( event->direction == GDK_SCROLL_DOWN && 1 < quality ) { quality -= change; }
	char str[256];
	sprintf(str, "mandel - quality = %u", quality);
	gtk_window_set_title(GTK_WINDOW(window), str);
	return true;
}

void MandelGUI::OnImageCompletion(MandelProvider* provider, MandelImage* image)
{
	pthread_mutex_lock(&queuelock);
	numscheduled--;
	pthread_mutex_unlock(&queuelock);

	QueueImage(image);
}

void MandelGUI::OnImageDiscarded(MandelProvider* provider, MandelImage* image)
{
	pthread_mutex_lock(&queuelock);
	numscheduled--;
	pthread_mutex_unlock(&queuelock);

	delete image;
}

void MandelGUI::QueueImage(MandelImage* image)
{
	pthread_mutex_lock(&queuelock);
	if ( queuelast ) { queuelast->next = image; }
	queuelast = image;
	if ( !queuefront ) { queuefront = queuelast; }
	pthread_mutex_unlock(&queuelock);

	gdk_threads_enter();
	QueueRedraw();
	gdk_threads_leave();
}

