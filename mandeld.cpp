#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <error.h>

size_t innermost = 0;

static inline unsigned DrawMandelPoint(unsigned maxiters, float x0, float y0)
{
	float x = x0;
	float y = y0;
	unsigned i;
	for ( i = 1; x*x + y*y <= 4.0f && i <= maxiters; i++ )
	{
		innermost++;
		float newx = x*x - y*y + x0;
		float newy = 2*x*y + y0;
		x = newx;
		y = newy;
	}
	if ( maxiters < i ) { return 0; }
	return i;
}

static void DrawMandelSet(unsigned maxiters, unsigned xres, unsigned yres, unsigned* frame, float xoffset, float yoffset, float xwidth, float ywidth)
{
	const unsigned INROWTHRES = 3U;
	const unsigned INROWSKIP = 2U;
	for ( unsigned ypixel = 0; ypixel < yres; ypixel++ )
	{
		const float y = yoffset + (ypixel * ywidth) / yres;
		unsigned inrow = 0;
		for ( unsigned xpixel = 0; xpixel < xres; xpixel++ )
		{
			const size_t index = ypixel * yres + xpixel;
			const float x = xoffset + (xpixel * xwidth) / xres;
			const unsigned result = DrawMandelPoint(maxiters, x, y);
			frame[index] = result;
#if 1
			if ( result )
			{
				if ( INROWTHRES <= inrow )
				{
					xpixel -= INROWSKIP;
				}
				inrow = 0;
			}
			else if ( INROWTHRES <= ++inrow && INROWSKIP <= xres - xpixel )
			{
				for ( unsigned i = 0; i < INROWSKIP; i++ )
				{
					frame[index + i] = 0; xpixel++;
				}
			}
#endif
		}
	}
}

static void MandelColorize(unsigned maxiters, unsigned xres, unsigned yres, const unsigned* data, unsigned* image)
{
	for ( unsigned y = 0; y < yres; y++ )
	{
		for ( unsigned x = 0; x < xres; x++ )
		{
			const size_t index = y * yres + x;
			unsigned datum = data[index];
			unsigned red = datum;
			unsigned blue = datum / 2;
			unsigned green = datum / 4;
			unsigned pixel = (red << 16U) | (blue << 8U) | (green << 0U);
			image[index] = pixel;
		}
	}
}

struct __attribute__ ((__packed__)) BMPFileHeader
{
	char signature[2];
	uint32_t filesize;
	uint32_t reserved;
	uint32_t dataoffset;
};

struct __attribute__ ((__packed__)) BMPInfoHeader
{
	uint32_t size;
	uint32_t width;
	uint32_t height;
	uint16_t numplanes;
	uint16_t bitcount;
	uint32_t compression;
	uint32_t imagesize;
	uint32_t xpixelspermeter;
	uint32_t ypixelspermeter;
	uint32_t numcolorsused;
	uint32_t numimportantcolors;
};

struct BMPHeader
{
	BMPFileHeader file;
	BMPInfoHeader info;
};

static bool WriteAll(int fd, const void* buf, size_t size)
{
	const uint8_t* buffer = (const uint8_t*) buf;
	size_t sofar = 0;
	while ( sofar < size )
	{
		size_t available = size - sofar;
		ssize_t numbytes = write(fd, buffer + sofar, available);
		if ( numbytes <= 0 ) { return false; }
		sofar += numbytes;
	}
	return true;
}

static bool SaveMandelSet(const char* path, unsigned maxiters, unsigned xres, unsigned yres, float xoffset, float yoffset, float xwidth, float ywidth)
{
	int fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0666);
	if ( fd < 0 ) { return false; }

	const size_t imagesize = sizeof(unsigned) * xres * yres;
	unsigned* data = new unsigned[xres * yres];
	unsigned* image = new unsigned[xres * yres];

	DrawMandelSet(maxiters, xres, yres, data, xoffset, yoffset, xwidth, ywidth);
	MandelColorize(maxiters, xres, yres, data, image);

	BMPHeader bmp;
	bmp.file.signature[0] = 'B';
	bmp.file.signature[1] = 'M';
	bmp.file.filesize = sizeof(bmp) + imagesize;
	bmp.file.reserved = 0;
	bmp.file.dataoffset = sizeof(bmp);
	bmp.info.size = sizeof(bmp.info);
	bmp.info.width = xres;
	bmp.info.height = yres;
	bmp.info.numplanes = 1;
	bmp.info.bitcount = 32;
	bmp.info.compression = 0;
	bmp.info.imagesize = 0;
	bmp.info.xpixelspermeter = 0;
	bmp.info.ypixelspermeter = 0;
	bmp.info.numcolorsused = 0;
	bmp.info.numimportantcolors = 0;


	bool result = false;

	if ( !WriteAll(fd, &bmp, sizeof(bmp)) ) { goto out; }
	if ( !WriteAll(fd, image, imagesize) ) { goto out; }

	result = true;

out:
	close(fd);

	delete[] data;
	delete[] image;

	return result;
}

static void Benchmark()
{
	const unsigned maxiters = 255;
	const unsigned xres = 768;
	const unsigned yres = 768 / 32;
	const float xoffset = -2.0f;
	const float yoffset = -2.0f;
	const float xwidth = 4.0f;
	const float ywidth = 4.0f;
	unsigned* data = new unsigned[xres * yres];
	const size_t NUMTESTS = 100;
	for ( size_t i = 0; i < NUMTESTS; i++ )
	{
		DrawMandelSet(maxiters, xres, yres, data, xoffset, yoffset, xwidth, ywidth);
	}
	printf("tests = %zu, innermost = %zu\n", NUMTESTS, innermost / NUMTESTS);
}

int main(int argc, char* argv[])
{
#if 1
	Benchmark();
	return 0;
#else
	if ( !SaveMandelSet("mandel.bmp", 255, 768, 768, -2.0f, -2.0f, 4.0f, 4.0f) )
	{
		error(1, errno, "SaveMandelSet");
	}
	return 0;
#endif
}

