#ifndef MANDELPROVIDERTHREADED_H
#define MANDELPROVIDERTHREADED_H

#include <pthread.h>
#include "mandelprovider.h"

class MandelGenerator;
struct MandelProviderThreadParams;

class MandelProviderThreaded : public MandelProvider
{
public:
	MandelProviderThreaded(MandelGenerator* gen, unsigned numthreads);
	virtual ~MandelProviderThreaded();
	virtual void MainLoop();
	virtual void Schedule(MandelImage* image);
	virtual void ClearSchedule();
	virtual void Halt();
	virtual void SetListener(MandelProviderListener* listener);

/*private*/ public: // Consider these private
	void RunWorker(MandelProviderThreadParams* params);

private:
	pthread_mutex_t queuelock;
	pthread_cond_t queuecond;
	pthread_cond_t completioncond;
	MandelImage* queuefront;
	MandelImage* queueback;
	MandelImage** workerqueuesfront;
	MandelImage** workerqueuesback;
	MandelGenerator* gen;
	MandelProviderListener* listener;
	unsigned numthreads;
	unsigned threadswaiting;
	bool waitingforcompletion;

};

#endif
