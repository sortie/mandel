#ifndef MANDELPROVIDERLOCAL_H
#define MANDELPROVIDERLOCAL_H

#include <pthread.h>
#include "mandelgenerator.h"
#include "mandelprovider.h"

class MandelProviderLocal : public MandelProvider
{
public:
	MandelProviderLocal(MandelGenerator* gen);
	virtual ~MandelProviderLocal();
	virtual void MainLoop();
	virtual void Schedule(MandelImage* image);
	virtual void ClearSchedule();
	virtual void Halt();
	virtual void SetListener(MandelProviderListener* listener);

private:
	MandelImage* Pop();
	void Push(MandelImage* image);
	void Provide(MandelImage* image);
	void Discard(MandelImage* image);

private:
	MandelImage* queuefront;
	MandelImage* queueback;
	pthread_mutex_t queuelock;
	pthread_cond_t queuecond;
	MandelGenerator* gen;
	MandelProviderListener* listener;
	volatile bool running;
	bool waitingforrequest;

};

#endif
