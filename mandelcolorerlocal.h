#ifndef MANDELCOLORERLOCAL_H
#define MANDELCOLORERLOCAL_H

#include "mandelcolorer.h"

class MandelColorerLocal : public MandelColorer
{
public:
	MandelColorerLocal();
	virtual ~MandelColorerLocal();
	virtual void Color(MandelImage* image);
};

#endif
