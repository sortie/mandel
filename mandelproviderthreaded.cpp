#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include "mandelimage.h"
#include "mandelgenerator.h"
#include "mandelprovider.h"
#include "mandelproviderthreaded.h"

struct MandelProviderThreadParams
{
	unsigned id;
	MandelProviderThreaded* me;
};

void* MandelProviderThreaded__RunWorker(void* user)
{
	MandelProviderThreadParams* params = (MandelProviderThreadParams*) user;
	params->me->RunWorker(params);
	return NULL;
}

MandelProviderThreaded::MandelProviderThreaded(MandelGenerator* gen, unsigned numthreads)
{
	this->gen = gen;
	this->listener = NULL;
	this->numthreads = numthreads;
	this->threadswaiting = 0;
	this->queuefront = NULL;
	this->queueback = NULL;
	this->workerqueuesfront = new MandelImage*[numthreads];
	this->workerqueuesback = new MandelImage*[numthreads];
	this->waitingforcompletion = false;
	pthread_mutex_init(&queuelock, NULL);
	pthread_cond_init(&queuecond, NULL);
	pthread_cond_init(&completioncond, NULL);

	for ( unsigned i = 0; i < numthreads; i++ )
	{
		workerqueuesfront[i] = NULL;
		workerqueuesback[i] = NULL;
		MandelProviderThreadParams* params = new MandelProviderThreadParams;
		params->id = i;
		params->me = this;
		pthread_t thread;
		pthread_create(&thread, NULL, MandelProviderThreaded__RunWorker, params);
	}
}

MandelProviderThreaded::~MandelProviderThreaded()
{
	// TODO: Wait for threads to stop around here.
	pthread_cond_destroy(&completioncond);
	pthread_cond_destroy(&queuecond);
	pthread_mutex_destroy(&queuelock);
	// TODO: Discard and delete anything in the queue.
	ClearSchedule();
	delete gen;
}

void MandelProviderThreaded::MainLoop()
{
	// TODO: Terminate when requested
	while ( true )
	{
		pthread_mutex_lock(&queuelock);
		while ( !queuefront || queuefront->chunksleft )
		{
			waitingforcompletion = true;
			pthread_cond_wait(&completioncond, &queuelock);
		}
		waitingforcompletion = false;
		MandelImage* image = queuefront;
		queuefront = image->next;
		if ( !queuefront ) { queueback = NULL; }
		pthread_mutex_unlock(&queuelock);
		image->next = NULL;
		if ( listener ) { listener->OnImageCompletion(this, image); }
		else { delete image; }
	}
}

void MandelProviderThreaded::Schedule(MandelImage* image)
{
	pthread_mutex_lock(&queuelock);
	image->next = NULL;
	if ( queueback ) { queueback->next = image; }
	queueback = image;
	if ( !queuefront ) { queuefront = image; }
	image->chunksleft = numthreads;
	for ( unsigned i = 0; i < numthreads; i++ )
	{
		// Divide the task evenly into tasks for each thread.
		MandelImage* workerimage = new MandelImage(image);
		workerimage->lineoffset += i * image->lineskip;
		workerimage->lineskip *= numthreads;
		if ( workerqueuesfront[i] ) { workerqueuesback[i]->next = workerimage; }
		workerqueuesback[i] = image;
		if ( !workerqueuesfront[i] ) { workerqueuesfront[i] = workerimage; }
	}
	if ( threadswaiting )
	{
		threadswaiting = 0;
		pthread_cond_broadcast(&queuecond);
	}
	pthread_mutex_unlock(&queuelock);
}

void MandelProviderThreaded::RunWorker(MandelProviderThreadParams* params)
{
	unsigned id = params->id;
	// TODO: Terminate when requested
	while ( true )
	{
		pthread_mutex_lock(&queuelock);
		while ( !workerqueuesfront[id] )
		{
			threadswaiting++;
			pthread_cond_wait(&queuecond, &queuelock);
		}

		MandelImage* image = workerqueuesfront[id];
		workerqueuesfront[id] = image->next;
		if ( !workerqueuesfront[id] ) { workerqueuesback[id] = NULL; }
		pthread_mutex_unlock(&queuelock);

		gen->Generate(image);

		// If we finished the image, wake the mainloop.
		pthread_mutex_lock(&queuelock);
		if ( --image->GetInner()->chunksleft == 0 && waitingforcompletion )
		{
			pthread_cond_signal(&completioncond);
		}
		pthread_mutex_unlock(&queuelock);

		delete image;
	}

	delete params;
}

void MandelProviderThreaded::ClearSchedule()
{
	// TODO: Implement this.
}

void MandelProviderThreaded::Halt()
{
	// TODO: Implement this.
}

void MandelProviderThreaded::SetListener(MandelProviderListener* listener)
{
	this->listener = listener;
}


