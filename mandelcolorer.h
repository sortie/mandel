#ifndef MANDELCOLORER_H
#define MANDELCOLORER_H

class MandelImage;

class MandelColorer
{
public:
	virtual ~MandelColorer() { }
	virtual void Color(MandelImage* image) = 0;
};

#endif
