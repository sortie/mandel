#include <stdlib.h>
#include <stdint.h>
#include "mandelimage.h"

MandelImage::MandelImage(size_t stride, unsigned width, unsigned height)
{
	this->inner = NULL;
	this->stride = stride;
	this->width = width;
	this->height = height;
	this->next = NULL;
	this->xoffset = -1.0;
	this->yoffset = -1.0;
	this->xlength = 2.0;
	this->ylength = 2.0;
	this->maxiters = 255;
	this->lineoffset = 0;
	this->lineskip = 1;
	this->chunksleft = 0;
	this->data = new uint8_t[stride * height];
}

MandelImage::MandelImage(MandelImage* inner)
{
	this->inner = inner;
	this->stride = inner->GetStride();
	this->width = inner->GetWidth();
	this->height = inner->GetHeight();
	this->next = NULL;
	this->xoffset = inner->xoffset;
	this->yoffset = inner->yoffset;
	this->xlength = inner->xlength;
	this->ylength = inner->ylength;
	this->maxiters = inner->maxiters;
	this->lineoffset = inner->lineoffset;
	this->lineskip = inner->lineskip;
	this->chunksleft = 0;
	this->data = inner->GetData();
}

MandelImage::~MandelImage()
{
	if ( !inner ) { delete[] data; }
}
