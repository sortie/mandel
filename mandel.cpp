#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "mandelgenerator.h"
#include "mandelgeneratorlocal.h"
#include "mandelprovider.h"
#include "mandelproviderlocal.h"
#include "mandelproviderthreaded.h"
#include "mandelcolorer.h"
#include "mandelcolorerlocal.h"
#include "mandelgui.h"

bool MakeUI()
{
	MandelColorer* colorer = new MandelColorerLocal();
	MandelGenerator* gen = new MandelGeneratorLocal();
	MandelProvider* provider;
	long numcpus = sysconf(_SC_NPROCESSORS_ONLN);
	if ( numcpus < 2 ) { provider = new MandelProviderLocal(gen); }
	else { provider = new MandelProviderThreaded(gen, numcpus); }
	MandelGUI* gui = new MandelGUI(colorer, provider, 800, 600);
	return true;
}

int main(int argc, char* argv[])
{
	g_thread_init(NULL);
	gdk_threads_init();
	gtk_init(&argc, &argv);

	MakeUI();

	gdk_threads_enter();
	gtk_main();
	gdk_threads_leave();

	return 0;
}
