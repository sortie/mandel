CFLAGS=-O3
CXXFLAGS=-O3

GTKVERSION:=2

LIBARIES=gtk+-$(GTKVERSION).0 gdk-$(GTKVERSION).0 glib-2.0 cairo
BINARIES=mandel mandeld
CPPFLAGS=`pkg-config --cflags $(LIBARIES)`
LDLIBS=`pkg-config --libs $(LIBARIES)` -lstdc++

all: $(BINARIES)
	
mandel: mandel.o mandelgui.o mandelproviderlocal.o mandelcolorerlocal.o \
        mandelgeneratorlocal.o mandelimage.o mandelproviderthreaded.o

mandeld: mandeld.o

%.o: %.cpp *.h

clean:
	rm -f *.o 

run: mandel
	./mandel

