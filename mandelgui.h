#ifndef MANDELGUI_H
#define MANDELGUI_H

#include <cairo.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "mandelcolorer.h"
#include "mandelcontroller.h"

class MandelImage;

class MandelGUI : public MandelProviderListener
{
public:
	MandelGUI(MandelColorer* colorer, MandelProvider* provider,
	          unsigned windowwidth, unsigned windowheight);
	virtual ~MandelGUI();

public:
	virtual void OnImageCompletion(MandelProvider* provider, MandelImage* image);
	virtual void OnImageDiscarded(MandelProvider* provider, MandelImage* image);

/*private:*/ public: // Consider these private.
	bool OnButtonPress(GtkWidget* widget, GdkEventButton* event);
	bool OnDraw(GtkWidget* widget, cairo_t* cr);
	bool OnScroll(GtkWidget* widget, GdkEventScroll* event);

private:
	void QueueRedraw();
	void QueueImage(MandelImage* image);
	float LengthX() const;
	float LengthY() const;
	float OffsetX() const;
	float OffsetY() const;
	float TranslateX(unsigned x) const;
	float TranslateY(unsigned y) const;

private:
	uint8_t* Render(size_t stride, unsigned width, unsigned height);

private:
	MandelColorer* colorer;
	MandelProvider* provider;
	MandelImage* currentframe;
	GtkWidget* window;
	GtkWidget* screen;
	unsigned quality;
	unsigned windowwidth;
	unsigned windowheight;
	unsigned areawidth;
	unsigned areaheight;
	MandelImage* queuefront;
	MandelImage* queuelast;
	pthread_mutex_t queuelock;
	size_t numscheduled;
	float xcenter;
	float ycenter;
	float xwidth;

};

#endif
