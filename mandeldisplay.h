#ifndef MANDEL_DISPLAY
#define MANDEL_DISPLAY

class MandelDisplay {
public:
	virtual void OnScrollEvent(int direction, float x, float y) = 0;
	virtual void OnButtonEvent(int type, float x, float y) = 0;
	virtual float LengthX(float time) const = 0;
	virtual float LengthY(float time) const = 0;
	virtual float OffsetX(float time) const = 0;
	virtual float OffsetY(float time) const = 0;

};

#endif

