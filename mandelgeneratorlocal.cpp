#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "mandelimage.h"
#include "mandelgenerator.h"
#include "mandelgeneratorlocal.h"

MandelGeneratorLocal::MandelGeneratorLocal()
{
}

MandelGeneratorLocal::~MandelGeneratorLocal()
{
}

static inline unsigned DrawMandelPoint(unsigned maxiters, float x0, float y0)
{
	float x = x0;
	float y = y0;
	unsigned i;
	for ( i = 1; x*x + y*y <= 4.0f && i <= maxiters; i++ )
	{
		float newx = x*x - y*y + x0;
		float newy = 2*x*y + y0;
		x = newx;
		y = newy;
	}
	if ( maxiters < i ) { return 0; }
	return i;
}

static void DrawMandelLine(unsigned maxiters, size_t stride, unsigned xres,
                           float xoffset, float xlength, unsigned ypixel,
                           float y, unsigned* frame)
{
	stride /= sizeof(unsigned);
	const unsigned INROWTHRES = 3U;
	const unsigned INROWSKIP = 2U;
	unsigned inrow = 0;
	for ( unsigned xpixel = 0; xpixel < xres; xpixel++ )
	{
		const size_t index = ypixel * stride + xpixel;
		const float x = xoffset + (xpixel * xlength) / xres;
		const unsigned result = DrawMandelPoint(maxiters, x, y);
		frame[index] = result;
#if 1
		if ( result )
		{
			if ( INROWTHRES <= inrow ) { xpixel -= INROWSKIP + 1; }
			inrow = 0;
		}
		else if ( INROWTHRES <= ++inrow && INROWSKIP <= xres - xpixel )
		{
			for ( unsigned i = 1; i <= INROWSKIP; i++ )
			{
				frame[index + i] = 0; xpixel++;
			}
		}
#endif
	}
}

void MandelGeneratorLocal::Generate(MandelImage* image)
{
	unsigned maxiters = image->maxiters;
	size_t stride = image->GetStride();
	unsigned xres = image->GetWidth();
	unsigned yres = image->GetHeight();
	unsigned* data = (unsigned*) image->GetData();
	float xoffset = image->xoffset;
	float yoffset = image->yoffset + image->ylength;
	float xlength = image->xlength;
	float ylength = -image->ylength;
	unsigned lineoffset = image->lineoffset;
	unsigned lineskip = image->lineskip;
	for ( unsigned ypixel = lineoffset; ypixel < yres; ypixel += image->lineskip )
	{
		const float y = yoffset + (ypixel * ylength) / yres;
		DrawMandelLine(maxiters, stride, xres, xoffset, xlength, ypixel, y, data);
	}
}

