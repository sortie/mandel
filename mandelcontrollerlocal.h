#ifndef MANDELCONTROLLERLOCAL_H
#define MANDELCONTROLLERLOCAL_H

#include "mandelcontroller.h"

// Responsibilities:
// - Contact generator for frames and serve them when available, discard if needed.
// - Decide what should be shown at a given time.
// - Schedule a few frames in advance to be generated from the server.

class MandelControllerLocal : public MandelController, public MandelListener
{
public:
	MandelControllerLocal(MandelProvider* provider); 
	virtual ~MandelControllerLocal();
	virtual void OnScrollEvent(int direction, float x, float y);
	virtual void OnButtonEvent(int type, float x, float y);
	virtual void OnResize(unsigned width, unsigned height);
	virtual unsigned GetWidth() const;
	virtual unsigned GetHeight() const;
	virtual float LengthX() const;
	virtual float LengthY() const;
	virtual float OffsetX() const;
	virtual float OffsetY() const;
	virtual float TranslateX(unsigned x) const;
	virtual float TranslateY(unsigned y) const;
	virtual void MainLoop();
	virtual void Schedule(MandelImage* image);
	virtual void ClearSchedule();
	virtual void Halt();
	virtual void SetListener(MandelProviderListener* listener);
	virtual void OnImageCompletion(MandelProvider* provider, MandelImage* image);
	virtual void OnImageDiscarded(MandelProvider* provider, MandelImage* image);

public:
	MandelProvider* provider;
	MandelProviderListener* listener;

};

#endif
