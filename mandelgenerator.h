#ifndef MANDELGENERATOR_H
#define MANDELGENERATOR_H

class MandelImage;

class MandelGenerator
{
public:
	virtual ~MandelGenerator() { }
	virtual void Generate(MandelImage* image) = 0;

};

#endif

