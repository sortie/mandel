#ifndef MANDELPROVIDER_H
#define MANDELPROVIDER_H

class MandelImage;
class MandelProvider;
class MandelProviderListener;

class MandelProvider
{
public:
	virtual ~MandelProvider() { }
	virtual void MainLoop() = 0;
	virtual void Schedule(MandelImage* image) = 0;
	virtual void ClearSchedule() = 0;
	virtual void Halt() = 0;
	virtual void SetListener(MandelProviderListener* listener) = 0;

};

class MandelProviderListener
{
public:
	virtual ~MandelProviderListener() { }
	virtual void OnImageCompletion(MandelProvider* provider, MandelImage* image) = 0;
	virtual void OnImageDiscarded(MandelProvider* provider, MandelImage* image) = 0;

};

#endif
