#include <stdlib.h>
#include <stdint.h>
#include "mandelimage.h"
#include "mandelcolorer.h"
#include "mandelcolorerlocal.h"

MandelColorerLocal::MandelColorerLocal()
{
}

MandelColorerLocal::~MandelColorerLocal()
{
}

void MandelColorerLocal::Color(MandelImage* image)
{
	unsigned* data = (unsigned*) image->GetData();
	size_t stride = image->GetStride() / sizeof(unsigned);
	unsigned xres = image->GetWidth();
	unsigned yres = image->GetHeight();
	unsigned maxiters = image->maxiters;
	for ( unsigned y = 0; y < yres; y++ )
	{
		for ( unsigned x = 0; x < xres; x++ )
		{
			const size_t index = y * stride + x;
			unsigned datum = (data[index] * 255U) / maxiters;
			unsigned red = datum;
			unsigned blue = datum / 2;
			unsigned green = datum / 4;
			unsigned pixel = (red << 16U) | (blue << 8U) | (green << 0U);
			data[index] = pixel;
		}
	}
}
