#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include "mandelimage.h"
#include "mandelprovider.h"
#include "mandelproviderlocal.h"

MandelProviderLocal::MandelProviderLocal(MandelGenerator* gen)
{
	this->gen = gen;
	this->running = true;
	this->listener = NULL;
	this->queuefront = NULL;
	this->queueback = NULL;
	this->waitingforrequest = false;
	pthread_mutex_init(&queuelock, NULL);
	pthread_cond_init(&queuecond, NULL);
}

MandelProviderLocal::~MandelProviderLocal()
{
	ClearSchedule();
	pthread_cond_destroy(&queuecond);
	pthread_mutex_destroy(&queuelock);
	delete gen;
}

void MandelProviderLocal::MainLoop()
{
	running = true;
	while ( running )
	{
		MandelImage* image = Pop();
		if ( !running ) { Discard(image); return; }
		gen->Generate(image);
		Provide(image);
	}
}

void MandelProviderLocal::Schedule(MandelImage* image)
{
	Push(image);
}

void MandelProviderLocal::ClearSchedule()
{
	pthread_mutex_lock(&queuelock);
	while ( queuefront )
	{
		MandelImage* image = queuefront;
		queuefront = image->next;
		Discard(image);
	}
	pthread_mutex_unlock(&queuelock);
}

MandelImage* MandelProviderLocal::Pop()
{
	pthread_mutex_lock(&queuelock);
	while ( !queuefront )
	{
		waitingforrequest = true;
		pthread_cond_wait(&queuecond, &queuelock);
	}
	waitingforrequest = false;
	MandelImage* result = queuefront;
	queuefront = result->next;
	if ( !queuefront ) { queueback = NULL; }
	pthread_mutex_unlock(&queuelock);
	result->next = NULL;
	return result;
}

void MandelProviderLocal::Push(MandelImage* image)
{
	pthread_mutex_lock(&queuelock);
	if ( queueback ) { queueback->next = image; }
	queueback = image;
	if ( !queuefront ) { queuefront = image; }
	if ( waitingforrequest ) { pthread_cond_signal(&queuecond); }
	pthread_mutex_unlock(&queuelock);
}

void MandelProviderLocal::Provide(MandelImage* image)
{
	if ( listener ) { listener->OnImageCompletion(this, image); }
	else { delete image; }
}

void MandelProviderLocal::Discard(MandelImage* image)
{
	if ( listener ) { listener->OnImageDiscarded(this, image); }
	else { delete image; }
}

void MandelProviderLocal::Halt()
{
	pthread_mutex_lock(&queuelock);
	running = false;
	pthread_mutex_unlock(&queuelock);
}

void MandelProviderLocal::SetListener(MandelProviderListener* listener)
{
	this->listener = listener;
}

