#ifndef MANDELGENERATORLOCAL_H
#define MANDELGENERATORLOCAL_H

#include "mandelgenerator.h"

class MandelGeneratorLocal : public MandelGenerator
{
public:
	MandelGeneratorLocal();
	virtual ~MandelGeneratorLocal();
	virtual void Generate(MandelImage* image);

};

#endif

